import {square} from './modules/areaOfSquare.js';
import {calcDiscount} from './modules/discountedPrice.js';
import {travelTime} from './modules/travelTime.js';
import {secondsInYear} from './modules/secondsInaYear.js';


if (process.argv[2].toLowerCase().includes("square")) {
    square(process.argv[3]);
} else if (process.argv[2].toLowerCase().includes("discount")){
    calcDiscount(process.argv[3], process.argv[4]);
} else if (process.argv[2].toLowerCase().includes("year")){
    secondsInYear();
} else if (process.argv[2].toLowerCase().includes("time")){
    travelTime(process.argv[3], process.argv[4]);
} else if (process.argv[2].toLowerCase().includes("help")){
    console.log("\nType one of following commands:\n");
    console.log("1)\tGet the squarerooth of a number: \n\tnode index.js square <yuor_number>\n");
    console.log("2)\tCalculate the discounted price: \n\tnode index.js discount <price> <percentage>\n");
    console.log("3)\tGet the number of seconds in a year: \n\tnode index.js year\n");
    console.log("4)\tCalculate time to destination in hours: \n\tnode index.js time <kilometers> <km/h>\n");
} else {
    console.log("Something went wrong. Try again.. (Get help with 'node index.js help' command)\n");
}