
const calcDiscount = (val, disc) => {
    const price = (val);
    const discount = (disc);
    const discountInDecimals = 1 - (discount / 100);
    const result = price * discountInDecimals;

    console.log("Price is =", price);
    console.log("Discount is =", discount, "%");
    console.log("calculation =", price, "*", discountInDecimals, "=", result);
}

export {calcDiscount};
