const price = (process.argv[2]);
const discount = (process.argv[3]);
const discountInDecimals = 1 - (process.argv[3] / 100);
const result = price * discountInDecimals;

console.log("Price is =", price);
console.log("Discount is =", discount);
console.log("calculation =", price, "*", discountInDecimals, "=", result);
