module.exports = {
  env: {
    browser: false,
    commonjs: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 13,
  },
  rules: {
    "indent": ["error", 2], // changed from 4 to 2
    "linebreak-style": ["error", "windows"],
    "quotes": ["error", "double"],
    "semi": ["error", "always"],
    "no-var": [
        "error"
    ],
    "no-unused-vars": [
        "error"
    ],
    "no-const-assign": [
        "error"
    ],
    "function-paren-newline": [
        "error", 
        "never"
    ],
    eqeqeq: "error",
    },
};
