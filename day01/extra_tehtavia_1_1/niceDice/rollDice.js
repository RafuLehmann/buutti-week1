/**
 * GAME RULES 
 * 
 * - Dietype is entered as a parameter when starting the program X
 * - App should not accept other arguments than dice type when it is launched. X
 * - Allowed dice sizes D3-D100 (D100 means 100-sided dice)
 * - If player selects a dice of a size smaller than D3 the app should change it to the default, D6.
 * - The app is quited by pressing 'q' on the standard input.
 * - The dice is rolled by typing anything else on the standard input.
 *
 *  
 *      RESULTS:
 * 
 *      <MIN>, message is: "Sad!"
 *      <MAX>, message is: "Happy!"
 *      Anything else, message is: "You rolled <NUM>. try again!"
 * 
 * */


let inputVal = process.argv[2];
const regEx = /^D(([1-9])|([1-9][0-9])|(100))$/;
const diceSize = inputVal.slice(1);



/**
 * Function that rolles the dice => selects a random value between 
 * given max and min. Then returns the result.
 */

const rollDice = (max) => {
    const min = 1;
    let rolledNumber = Math.floor(Math.random() * max);
    
    if (rolledNumber === max) {
        return "Wohoo! You got "+rolledNumber+"!";
    } else if (rolledNumber === min) {
        return "Not you lucky day! You got "+rolledNumber+".";
    } else {
        return "You rolled "+rolledNumber+". Better luck next time!";
    }
}


/**
 * Requesting user to input the quessed value to the standard input.
 * if user hits 'q' + <enter> the dice will roll, else the dice will roll. 
 */

const guessNum = (size) => {
    let x = require('readline');
    let z = x.createInterface({
        input: process.stdin,
        output: process.stdout
    })
    z.question(`\nTry to roll maximum number ${size} \nRoll the dice: Press <ENTER>\nQuit the game: 'q' + <ENTER>.\n`, (y) => {
        if (y  === 'q') {
            console.log("Turning the game off now. Thank you for playing!")
            z.close();
        } else {
            console.log(rollDice(size));
            guessNum(size);
        }  
    })
    
}

/**
 * Validatin input before staring the game.
 */

if (!regEx.test(inputVal)) {
    console.error("Something went wrong, please run the command again.")
} else {
    const size = (diceSize < 3) ? 6 : diceSize;
    guessNum(size);
}


