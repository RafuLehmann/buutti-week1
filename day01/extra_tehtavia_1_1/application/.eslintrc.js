module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 13,
  },
  rules: {
    "indent": ["error", 4], // changed from 4 to 2
    "linebreak-style": ["error", "windows"],
    "quotes": ["error", "single"],
    "semi": ["error", "always"],
    "no-var": [
        "error"
    ],
    "no-unused-vars": [
        "error"
    ],
    "no-const-assign": [
        "error"
    ],
    "function-paren-newline": [
        "error", 
        "never"
    ],
    "eqeqeq": "error",
    "no-console": 0
  },
};
