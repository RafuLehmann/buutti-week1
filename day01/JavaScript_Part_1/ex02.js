const a = 1;
const b = 3;

let sum = a + b;
let diff = a - b;
let fraction = a / b;
let product = a * b;
let exp = a ** b;
let mod = a % b;
console.log("let's calculate a("+a+") and b("+b+"):")
console.log("sum:",sum);
console.log("difference:", diff);
console.log("fraction:",fraction);
console.log("product:",product);
console.log("exp:", exp);
console.log("mod:", mod);