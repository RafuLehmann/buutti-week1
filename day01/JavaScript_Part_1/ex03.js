const str1 = "Combine";
const str2 = "Me";
const strSum = str1 + str2;
const str1Len = str1.length;
const str2Len = str2.length;
const avgLen = strSum.length/2;


console.log("\ninspecting str1 and str2:");
console.log("\n");
console.log("str1("+str1+"):"+str1Len+" characters.");
console.log("str1("+str2+"):"+str2Len+" characters.");
console.log("AVG length of the strings is:");
console.log(avgLen);
console.log("\n");
console.log("concat:", str1.concat(str2));
console.log("combine with plus", strSum);

if (str1Len > str2Len) {
    console.log("str1 is longer than str2.");
} else if(str1Len < str2Len) {
    console.log("str2 is longer than str1.");
}

if (strSum > 10) {
    console.log("the combine length of str1 and str 2 is longer than 10.");
}