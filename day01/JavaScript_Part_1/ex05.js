// const readLine = require("readline"):
// const rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
// })

// const 

let argument = process.argv;
//argument.shift();
//argument.shift();


/**
 * 
 *  Iterating thru all the chars of each argument, but skipping the first two 
 * 
 *  which contain the node and the file path. Pushing everything to a new array
 * 
 *  as one element.
 * 
 */

let modOutput = [];
for (let i = 2; i < argument.length; i++) {
    let j = 0;
    if (i !== 2) {
        modOutput = [];
    }
    
    if (argument[i].charAt(0) === " ") {
        console.log("\nWARNING: There should not be any white space in the beginning of the string => Removing the unnecessary whitespaces.");
        while (argument[i].charAt(0) === " ") {
            argument[i] = argument[i].substring(1);
        }
    }

    if (argument[i].charAt(argument[i].length-1) === " ") {
        console.log("WARNING: There should not be any white space in the end of the string => Removing the unnecessary whitespaces.");
        while (argument[i].charAt(argument[i].length-1) === " ") {
            argument[i] = argument[i].slice(0,-1);
        }
    }
    if (argument[i].charCodeAt(0) > 64 && argument[i].charCodeAt(0) <= 90) {
        console.log("WARNING: There should not be a Capital letter in the first word of your argument. Changing the letter to lowercase.");
        modOutput.push(argument[i][j].toLowerCase());
    } 

    while(j < argument[i].length) {
        if (modOutput.length === 20) {
            console.log("WARNING: Your argument exceeds the maximum length of 20 symbols. Returning only 20 first symbols.");
            break;
        } 
        else {
            modOutput.push(argument[i][j]);
        }
        j++;
    }
    console.log(modOutput.join("")+"("+modOutput.length+")");
    
}





