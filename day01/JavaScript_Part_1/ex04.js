const minPlayers = 3;
const isStressed = false;

if (minPlayers < 4) {
    console.log("a: Game not started. You need to be at least 4 players.")
} 


const hasIceCream = true;
const marksMood = isStressed || hasIceCream;

if (marksMood) {
    console.log("b: Mark is not stressed")
}

const degrees = 20;
const rain = false;
const sunShine = true;
const beachDay = !rain && (degrees >= 20) && sunShine;

if (beachDay) {
    console.log("c: time for beach!");
}

// exercise d:
const meetSuzy = false;
const meetDan = true;

const seeBoth = meetSuzy && meetDan;
const seeOne = meetSuzy || meetDan;

if(seeOne && !seeBoth) {
    console.log("d: Arin is happy.");
}