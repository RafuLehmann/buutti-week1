/******************************************************
 *
 *  A program that converts your name to initials, 
 *  separating them with a dot. Max allowed names 
 *  are 3. 
 *
 * ****************************************************/

const fullName = process.argv.slice(2)


/**
 * Function that converts the names into initials separated with a dot.
 */

const nameToInitials = (fullNameArr) => {
    return fullNameArr.map(name => name.slice(0,1)).join(".");
}

/**
 * Validates the input. Checks if there is any input and
 * that the amount of names are no more than 3. Returns true or false
 */

const validateInput = (fullName) => {
    
    if (fullName.length < 1) {
        console.error("You haven't given any names. Please input up to 3 names.");
        return false;
    } else if (fullName.length > 3) {
        console.error("Please input no more than 3 names.");
        return false;
    }
    return true;
}

/**
 * If input passes validation, initials will be printed
 */
if (validateInput(fullName)) {
    console.log(nameToInitials(fullName));
}