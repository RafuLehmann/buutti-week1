const first = process.argv[2];
const second = process.argv[3];
const third = process.argv[4];

// validating input
const validateThreeNums = (a, b , c) => {
    let errMsg = "";
    let result = "";

    if (!a || !b || !c){
        errMsg =  "\nCatched an error. Please try again."
        result = "\nType 'node greater_smaller_equal.js <value1> <value2> <value3>'";
        return (errMsg+result);

    } else if (a > b && a > c) {
        if (b < c) {
            result = "First value is the largest,\nsecond value is the smallest.";
        } else {
            result = "First value is the largest,\nthird value is the smallest.";
        }
        return (result);

    } else if (b > a && b > c) {
        if (a < c) {
            result = "Second value is the largest,\nfirst value is the smallest.";
        } else {
            result = "Second value is the largest,\nthird value is the smallest.";
        }
        return (result);

    } else if (c > a && c > b) {
        if (a < b) {
            result = "Third value is the largest,\nfirst value is the smallest.";
        } else {
            result = "Third value is the largest,\nsecond value is the smallest.";
        }
        return (result);
        
    } else {
        return "All values are equal."
    }
}

console.log(validateThreeNums(first, second, third));