/**
 * 
 * A program to check your balance. 
 * 
 * Create variables: 
 *  - balance
 *  - isActive
 *  - checkBalance
 * 
 * Write a conditional statement that implements the flow chart below.
 * 
 * Change the values of balance, checkBalance, and isActive to test your code!
 * 
 * 
 * Using ATM
 * |
 * V
 * Check your balance? -- (yes) --> Account: active  -- (yes) --> Print: 
 * | (no)                           balance: > 0?                 balance.
 * V                                | (no)
 * Output:                          V
 * "Have a nice day!"               Account:         -- (yes) --> Print: "Your
 *                                  not active?                   account is not
 *                                  | (no)                        active."
 *                                  V
 *                                  Balance:         -- (yes) --> Print: "Your
 *                                  0                             account is empty."
 *                                  |(no)
 *                                  V
 *                                      
 */

let balance = 100;
let isActive = true;
let readLine = require('readline');
let rl = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
})


/**
 * Checking the if account is active and if there is balance
 */

const accountStatus = () => {

    if (isActive && balance > 0) { 
        console.log("Your balance is", balance);

    } else if (!isActive) {
        console.log("Your account is not active.");

    } else if (balance === 0) {
        console.log("Your account is empty.");

    } else {
        console.log("Your balance is negative.");

    }
    return rl.close();
}


/**
 * Main function: Making actions based on the input
 */

const atm = () => {
    rl.question("Check your balance?\n", (answer) => {
        if (answer.toLowerCase() === 'no') {
            console.log("Have a nice day!");
            rl.close();
        } else if (answer.toLowerCase() === 'yes') {
            accountStatus()
        } else {
            console.log("Something went wrong, please try again. Type 'yes' or 'no'." );
            atm();
        }   
    })

}
atm();