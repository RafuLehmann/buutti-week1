const lang = process.argv[2];


switch (lang) {
    case "fi":
        console.log("Terve Maailma!");
        break;
    case "en":
        console.log("Hello World!");
        break;
    case "es":
        console.log("Hola Mundo!");
        break;
    case "sv":
        console.log("Hej världen!");
        break;
    default:
        console.log("\nOops.. Something went wrong.");
        console.log("\nPlease select one of the following languages:");
        console.log("Finnish ('fi')\nEnglish ('en')\nSpanish ('es')\nFinnish ('fi')");
        console.log("\nExample command: node hello_world_p2.js fi");
}
