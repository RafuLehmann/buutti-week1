/**
 * Create a program that takes in 3 names, and compares the 
 * length of those names. Print out the names ordered so that 
 * the longest name is first. 
 * 
 * example: 
 * node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe
 */
let input = process.argv.slice(2);

input.sort( (a, b) => {
    return b.length - a.length;
})


console.log(input.join(" "))
