/**
 * A program that takes 2 inputs as argument and 
 * the first argument defines the action to be done,
 * either converting all letters to upper or lower case.
 */

const inputValues = process.argv.slice(2);

if (inputValues.length > 2) {
    console.log("Too many arguments.");
    console.log("Type command 'node modify_case.js <case> <your text>'.");

} else if (inputValues[0].toLowerCase() === "lower") {
    console.log(inputValues[1].toLowerCase());

} else if (inputValues[0].toLowerCase() === "upper") {
    console.log(inputValues[1].toUpperCase());

} else {
    console.log("Something went wrong. Try again.");
    console.log("Type command 'node modify_case.js <case> <your text>'.");
}
