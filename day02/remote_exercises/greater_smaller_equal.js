const a = process.argv[2];
const b = process.argv[3];

// validating input

if (!a || !b){
    console.log("\nCatched an error. Please try again.");
    console.log("Type 'node greater_smaller_equal.js <value1> >value2>'");
} else if (a > b) {
    console.log("a is greater");
} else if (a < b) {
    console.log("b is greater");
} else if (a === b) {
    console.log("they are equal");
}