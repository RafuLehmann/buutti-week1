const inputMonth = process.argv[2];
const months = ["January", "February", "March", 
"April", "May", "June", "July", 
"August", "September", "October", 
"Novermber", "December"]; 


/**
 * Function to extract the amount of days in given month of current year.
 */

const daysInGivenMonth = (month) => {
    const year = new Date().getFullYear();
    return new Date(year, month, 0).getDate();
}  

/**
 * Changing all months to lowercase to prevent 
 * unnecessary errors.
 */

const monthsInLowerCase = months.map(item => item.toLowerCase());


/**
 * Validating the type of input and converting 
 * the month to number if needed. Then printing 
 * the days int the given month.
 */

if (!inputMonth) {
    console.error("Your input is missing, please add the month in numbers or as a string. \nExample: 'February' or 2. ");  
} else if (!Number(inputMonth)) {
    if (!monthsInLowerCase.some(month => inputMonth.toLowerCase().includes(month.toLowerCase()))) {
        console.error("Your input is invalid, please add the month in numbers or as a string with capital letter.");
    } else {
        console.log("Your input", inputMonth, "is a string.")
        const monthInNumbers = monthsInLowerCase.indexOf(inputMonth.toLowerCase()) + 1;
        console.log("There is", daysInGivenMonth(monthInNumbers),"day(s) in that month.");
    }
} else {
    console.log("Your input", Number(inputMonth), "is a number.")
    console.log("There is", daysInGivenMonth(Number(inputMonth)),"day(s) in that month.");
};