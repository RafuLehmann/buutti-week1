const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7];

/* const largestNum = () => {
    let largest = 0;
    for (let i = 0; i <= arr.length; i++) {
        if (arr[i] > largest) {
            largest = arr[i];
        }
    }
    console.log(largest)
}
largestNum(); */

console.time("withLoops")
const scndlargestNumloop = () => {
    let largest = 0;
    let scnd = 0;
    const arr2 = arr.sort();
    for (let i = 0; i <= arr.length; i++) {
        if (arr[i] > largest) {
            largest = arr[i];
        }
    }
    console.log("largest", largest);
    
    for (let i = 0; i <= arr.length; i++) {
        if (arr[i] === largest ) {
            console.log("scnd", scnd)
            break;
        } else if (arr[i] > scnd) {
            scnd = arr[i];
        }
    }
}
scndlargestNumloop();
console.timeEnd("withLoops")
console.log(" ")

console.time("noLoops")

const scndlargestNum = () => {
    let largestx = 0;
    let scndx = 0;
    const arr2 = arr.sort();
    console.log(arr2)
    
    largestx = arr2.pop();
    scndx = arr2.pop();
    console.log("largest", largestx);
    console.log("scnd", scndx);
}
scndlargestNum();
console.timeEnd("noLoops")