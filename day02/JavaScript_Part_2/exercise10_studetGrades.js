/**
 * What this file does:
 * finds the highest, lowest scoring person.
 * finds the average score of the student.
 * Lists the students who scored higher than avg.
 * 
 * Assigns grades 1-5 for each student based on this grading rules:
 *  "1": "1-39"
 *  "2": "40-59"
 *  "3": "60-79"
 *  "4": "80-94"
 *  "5": "95-100"
 */

// default student table
const students = [
    {name: "markku", score: 99},
    {name: "susanna", score: 69},
    {name: "isak", score: 49},
    {name: "karoliina", score: 58},
    {name: "benjamin", score: 77},
    {name: "liisa", score: 89}
]

// student with highest value
let topScore = "";
let topStudent = "";

// student with lowest value
let rifRafScore = 0; 
let rifRafName = ""; 

let totScore = 0;
let studentCount = 0;
let avgScore = 0;


/**
 * Looking for the stundent with the highest and the lowest value. 
 * Counting also the amount of students and total scoring to get 
 * the average.
 */

for (key of students) {
    studentCount++;
    totScore += key["score"];
    if (topScore < key["score"]) {
        topScore = key["score"];
        topStudent = key["name"];
    }
    if (rifRafScore > key["score"] || rifRafScore === 0) {
        rifRafScore = key["score"];
        rifRafName = key["name"];
    }
}
avgScore = totScore / studentCount; 

console.log("Top Score:", topStudent+": "+topScore);
console.log("Lowest Score:", rifRafName+": "+rifRafScore);
console.log("Avg Score:", avgScore);


/**
 * Calculating the students with a score above the average.
 * Pushing them into a new array.
 */

let scoreAboveAvg = 0;
let nameAboveAvg = 0;
let aboveAvg = [];

for (key of students) {
    if (avgScore < key["score"]) {
        scoreAboveAvg = key["score"];
        nameAboveAvg = key["name"];
        aboveAvg.push({nameAboveAvg, scoreAboveAvg});
    }
}

console.log("\n")
console.log("Score above avg:", aboveAvg);


/**
 * Giving grades to each student based on their scores. 
 */

let studentGrades = students;
console.log("\n")
for (key of studentGrades) {
    switch (true) {
        case (1 <= key["score"] &&  39 >= key["score"]):
            key["grade"] = 1;
            break;
        case (40 <= key["score"] &&  59 >= key["score"]):
            key["grade"] = 2;
            break;
        case (60 <= key["score"] &&  79 >= key["score"]):
            key["grade"] = 3;
            break;
        case (80 <= key["score"] &&  94 >= key["score"]):
            key["grade"] = 4;
            break;
        case (95 <= key["score"] &&  100 >= key["score"]):
            key["grade"] = 5;  
            break; 
    }
}

console.log("Student grades:",studentGrades)
