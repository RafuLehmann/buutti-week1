const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

console.log("printing out all items");
arr.forEach(item => console.log(item));


console.log("\nprinting out item containing 'r'");
arr.filter(item => item.includes("r")).forEach(item => console.log(item));


console.log("\nprinting out all items sorted");
arr.sort().forEach(item => console.log(item));

let shiftArr = arr
shiftArr.shift()
console.log("\nprinting out all items except first");
shiftArr.forEach(item => console.log(item));


let pushArr = arr
pushArr.unshift("sipuli")
console.log("\nprinting out all items except first");
pushArr.forEach(item => console.log(item));

