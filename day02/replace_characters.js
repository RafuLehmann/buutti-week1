/**
 * Create a program that takes in a string, and replaces 
 * every occurrence of your given character with your 
 * other given character.
 * 
 * example: 
 * node .\replacecharacters.js g h "I have great grades for.." -> I have hreat hrades for..
 */

const input = process.argv[2];
const regEx = /a/g;
const output = input.replace(regEx, "b");
console.log(output);
